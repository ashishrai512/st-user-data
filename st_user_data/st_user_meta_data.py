from user_agents import parse
import streamlit as st
from streamlit.server.server import Server

#getting session id
try:
    #ugraded version of streamlit
    from streamlit.script_run_context import get_script_run_ctx
except ModuleNotFoundError:
    # streamlit < 1.4
    from streamlit.report_thread import (  # type: ignore
        get_report_ctx as get_script_run_ctx,
    )
def get_session_id() -> str:
    ctx = get_script_run_ctx()
    if ctx is None:
        raise Exception("Failed to get the session id")
    return ctx.session_id

#getting user meta data
def get_user_agent_stats(user_agent_info):
    if user_agent_info is not None:
        user_agent = parse(user_agent_info)
        user_agent_meta_data = {}
        # Accessing user agent's browser attributes
        user_agent_meta_data["browser"] = user_agent.browser.family
        # user_agent_meta_data["browser_version"] = user_agent.browser.version_string

        # Accessing user agent's operating system properties
        user_agent_meta_data["os_family"] = user_agent.os.family
        # user_agent_meta_data["os_version"] = user_agent.os.version_string

        # Accessing user agent's device properties
        user_agent_meta_data["device_family"] = user_agent.device.family
        user_agent_meta_data["device_brand"] = user_agent.device.brand
        # user_agent_meta_data["device_model"] = user_agent.device.model

        # sophisticated attributes
        user_agent_meta_data["is_mobile"] = user_agent.is_mobile
        # user_agent_meta_data["is_tablet"] = user_agent.is_tablet
        user_agent_meta_data["is_pc"] = user_agent.is_pc
        # user_agent_meta_data["is_touch_capable"] = user_agent.is_touch_capable
        # user_agent_meta_data["is_bot"] = user_agent.is_bot
        return user_agent_meta_data
    return None

#getting headers
def get_headers():
    try:
        # Hack to get the session object from Streamlit.
        current_server = Server.get_current()
        if hasattr(current_server, '_session_infos'):
            # Streamlit < 0.56
            session_infos = Server.get_current()._session_infos.values()
        else:
            session_infos = Server.get_current()._session_info_by_id.values()

        # Multiple Session Objects?

            for session_info in session_infos:
                if session_info.session.id == get_session_id():
                    headers = session_info.ws.request.headers
                    return str(headers['Origin']), str(headers["User-Agent"])
                else:
                    pass
    except Exception as e:
        print(e)


def get_user_meta_data():
    try:
        url_accessed, user_agent_info = get_headers()
        user_agent_meta_data = get_user_agent_stats(user_agent_info)
        user_agent_meta_data['session_id']= get_session_id()
        return user_agent_meta_data
    except:
        print("No header info found")
