import setuptools

setuptools.setup(
    name="st_user_data",
    version="0.0.1",
    author="Naga Budigam,Ashish Rai",
    author_email="nagaraju.iiit@gmail.com,ashishraics512@gmail.com",
    description="streamlit-user-data helps track the widget values in use & also gives user meta data",
    long_description="",
    long_description_content_type="text/plain",
    url="",
    license='MIT',
    packages=setuptools.find_packages(),
    include_package_data=True,
    classifiers=[],
    python_requires=">=3.6",
    install_requires=[
        "streamlit >= 0.63",
        "user-agents >= 2.2.0"
    ],
)
