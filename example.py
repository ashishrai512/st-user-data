import streamlit as st
import st_user_data as st_data

start_track=st_data.start_tracking(exclude_key=['is_select3'])
select1 = st.sidebar.selectbox(
    "Select the options",
    ['a','b','c'],
    key='is_select1',
)

select2=st.selectbox(
    "Select the options",
    ['aa', 'ba', 'ca'],
    key='is_select2',

)

select3=st.selectbox("select",
             options=[1,2,3],
                     key='is_select3')

st.write(st_data.get_user_meta_data())
st.write(start_track)
